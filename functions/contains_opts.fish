function contains_opts -d "Check if commandline contains any of the given options, or any other than these"
  set -l commandline (commandline -po)
  if not test "$argv"
    string match -qr -- '^--?[^-]+$' $commandline
    and return 0
  else
    for arg in $argv
      if string match -qr -- '^-+$' $arg
        return 1
      else if string match -qr -- '^-' $arg
        contains -- $arg $commandline
      else if string match -qr -- '^[^-]$' $arg
        string match -qr --  "^-[^-]*$arg"'[^-]*' $commandline
      else
        contains -- --$arg $commandline
      end
      or continue
      return 0
    end
  end
  return 1
end
